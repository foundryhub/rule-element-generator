function OpenApplication() {
    globalThis.RuleElementGenerator = new Application({
        width: 750,
        height: 600,
        popOut: true,
        minimizable: true,
        resizable: false,
        id: "rule-element-generator",
        template: "modules/rule-element-generator/application.html",
        title: "Rule Element Generator",
    }).render(true);
}

Hooks.on("renderItemSheet", (sheet, $html) => {
    const $EditButton = $('<a class="edit-rule-element" title="Edit Rule Element"><i class="fas fa-edit"></i></a>').on(
        "click",
        (event) => {
            globalThis.REG = {
                data: JSON.parse($(event.target).parent().parent().next().text()),
                source: $(event.target).parent().parent().next().attr("name"),
            };
            OpenApplication();
        }
    );
    $html.find("a.remove-rule-element").before($EditButton);
    $html.find(".rule-element-header").css("justify-content", "unset");
    $html.find("a.edit-rule-element").css({ "margin-left": "auto", "margin-right": 4 });

    const $AddButton = $(
        '<a class="add-rule-element" style="margin-left: 10px"><i class="fas fa-plus"></i> Rule Element Generator</a>'
    ).on("click", (event) => {
        event.preventDefault();
        globalThis.REG = {
            data: { key: "FlatModifier" },
            source: "New Rule Element",
            sheet,
        };

        OpenApplication();
    });
    $html.find("a.add-rule-element").after($AddButton);
});
