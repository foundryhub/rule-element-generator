# PF2e Rule Element Generator

An easy way to generate and edit rule elements for the Pathfinder 2e system in Foundry VTT.
To get started, go the an item's Rules tab (you must have Advanced Rule Element UI enabled in System Settings to see the tab), and click "Rule Element Generator" or an existing Rule Element's edit icon.
To learn more about rule elements go to https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/wikis/Going-Deeper-Topics/Quickstart-guide-for-rule-elements

### Manual Install

If you are unable to find this module within Foundry VTT's module browser, paste this into the Manifest URL section instead.

`https://gitlab.com/pearcebasmanm/rule-element-generator/-/raw/main/module.json`
